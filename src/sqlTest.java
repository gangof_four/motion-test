

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class sqlTest
 */
@WebServlet("/sqlTest")
public class sqlTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public sqlTest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		 PrintWriter out = response.getWriter();//[5]
	        out.println("<html><head></head><body>");
	        //out.println("aa");

		Connection con = null;
		PreparedStatement ps = null;

		try {

		Class.forName("com.mysql.cj.jdbc.Driver");

		//con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/db_sample?characterEncoding=UTF-8&serverTimezone=JST","root","");
		con = DriverManager.getConnection("jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST","testuser","1234");

		//String sql = "select * from m_student";
		String sql = "select * from questions";

		ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();

		while(rs.next()) {

			//String name = rs.getString("name");
			String contents = rs.getString("contents");
			//contents.replace("\r\n","<br>");

			//String maker = rs.getString("maker");

			out.println( "<p>" + contents.replace("\r\n","<br>") + "</p>" + "<br>");
			//out.println("<p>maker:" + maker + "</p>");
		}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(ps != null) {
					ps.close();
				}
				if(con != null) {
					con.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		out.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);


	}

}

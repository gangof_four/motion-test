//安田オンラインから
package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ChoicesListDAO;
import dao.QuestionsDAO;
import dao.QuestionsListDAO;
import model.Choices;
import model.Questions;
/**
 * Servlet implementation class QuestionsServlet
 */
@WebServlet("/QuestionsServlet")
public class QuestionsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.getWriter().append("Served at: ").append(request.getContextPath());

		HttpSession session = request.getSession();
		int category_id = (int) session.getAttribute("category_id");

		String nextLevel = request.getParameter("nextLevel");
		int nextLevel_int = Integer.parseInt(nextLevel);


		//選択した選択肢(choice_no)を配列に格納するための文字列userAnswerを用意
		String userAnswer = "";
		String[] select_no = (String[]) session.getAttribute("select_no");
		if(request.getParameterValues("choice_no") != null) {
			//複数の値をパラメーターで取得する場合の書き方
			String[] choice_no = request.getParameterValues("choice_no");
			for(String str : choice_no) {
				userAnswer += str;
			}
		}else {
			userAnswer = null;
		}

		int c_level_index = nextLevel_int;
		select_no[c_level_index] = userAnswer;


		//レビューチェックした場合に✔を配列に格納
		//MEMO 問題番号levelとインデックス番号level_index--が対応
		String[] review = (String[]) session.getAttribute("review");
		int level_index = nextLevel_int;
		if(request.getParameter("review_check") != null) {
			review[level_index] = "✔";
		}else {
			review[level_index] = null;
		}

		//次の問題へ遷移するためにlevelをインクリメントする
		nextLevel_int++;

		//次の問題のためのQuestionsインスタンスとChoicesのListを生成
		/*取得するレコードがなくなった場合（つまり次の問題がなくなりその章が終わったとき）、
		例外が起きるので例外処理で終了画面へ遷移*/
		//REVIEW 発生する例外を利用しているがこのままでいい？（LoginLogicの時のようにbooleanで判定する？）
		Questions questions  = new Questions(category_id,nextLevel_int) ;
		QuestionsDAO dao = new QuestionsDAO();
		try {
			Questions dao_questions = dao.findByQuestions(questions);
			ChoicesListDAO c = new ChoicesListDAO();
			List<Choices>choicesList = c.findByChoicesList(dao_questions);

			request.setAttribute("questions", dao_questions);
			request.setAttribute("choicesList", choicesList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/questions.jsp");
			dispatcher.forward(request, response);
		}catch(Exception e){
			//REVIEW all_questions.jspに遷移するためにAllQuestions.javaのコードをほぼ丸写ししたから長い！

			String nowLevel = request.getParameter("nextLevel");
			int nowLevel_int = Integer.parseInt(nowLevel);

			//問題の一覧を表示するために全レコードをQuestionsインスタンス化してListに格納
			Questions a_questions = new Questions(category_id,nowLevel_int);
			QuestionsListDAO q_tmp = new QuestionsListDAO();
			List<Questions>questionsList = q_tmp.findByQuestionsList(a_questions);


			request.setAttribute("questionsList", questionsList);
			request.setAttribute("nowLevel", nowLevel);

			//all_questions.jspに遷移
			String finish_message = "次の問題はありません。問題は以上となります。";
			request.setAttribute("finish_message", finish_message);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/all_questions.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		HttpSession session = request.getSession();
		int category_id = (int) session.getAttribute("category_id");

		//選択した問題番号の問題に遷移
		if(request.getParameter("thisLevel") != null) {

			String thisLevel = request.getParameter("thisLevel");
			int thisLevel_int = Integer.parseInt(thisLevel);

			Questions questions  = new Questions(category_id,thisLevel_int) ;
			QuestionsDAO dao = new QuestionsDAO();
			Questions dao_questions = dao.findByQuestions(questions);

			ChoicesListDAO c = new ChoicesListDAO();
			List<Choices>choicesList = c.findByChoicesList(dao_questions);

			request.setAttribute("questions", dao_questions);
			request.setAttribute("choicesList", choicesList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/questions.jsp");
			dispatcher.forward(request, response);

		//元の画面に戻る（遷移元と同じlevelを持ったQuestionsインスタンスをquestions.jspに渡す）
		}else if(request.getParameter("nowLevel") != null){
		String nowLevel = request.getParameter("nowLevel");
		int nowLevel_int = Integer.parseInt(nowLevel);

		Questions questions  = new Questions(category_id,nowLevel_int) ;
		QuestionsDAO dao = new QuestionsDAO();
		Questions dao_questions = dao.findByQuestions(questions);

		ChoicesListDAO c = new ChoicesListDAO();
		List<Choices>choicesList = c.findByChoicesList(dao_questions);

		request.setAttribute("questions", dao_questions);
		request.setAttribute("choicesList", choicesList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/questions.jsp");
		dispatcher.forward(request, response);
		}

	}

}

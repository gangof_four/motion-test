package servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ChoicesListDAO;
import dao.QuestionsDAO;
import model.Choices;
import model.Questions;

/**
 * Servlet implementation class FirstQuestions
 */
@WebServlet("/FirstQuestions")
public class FirstQuestions extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstQuestions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.getWriter().append("Served at: ").append(request.getContextPath());

		//loginUser.jspから受け取ったパラメーターを取得
		String category_id = request.getParameter("category_id");
		int category_id_int = Integer.parseInt(category_id);
		String nextLevel = request.getParameter("nextLevel");
		int nextLevel_int = Integer.parseInt(nextLevel);
		//解く問題の章をセッションにセット（解き終わったらセッションを切る）
		HttpSession session = request.getSession();
		session.setAttribute("category_id",category_id_int);


		//解答開始時間をセッションにセット（残り時間表示のために必要）
		//TODO タイムオーバーか最後の問題解答でstarting_timeセッションを切る
		//画面切り替えも必要（ここでするかquestions.jspで行うかはまだ不明）
		Date start = new Date();
		session.setAttribute("start", start);

		//レビュー（不安問題）チェックのための配列をセッションにセット（解き終わったらセッションを切る）
		//WARNING 要素数77が模試の問題数に相当するので章ごとの問題の場合は別途対応が必要(このままでも問題なく使うことはできる)
		String[] review = new String[78];
		session.setAttribute("review", review);

		//選択した選択肢を格納するための配列をセッションにセット
		String[] select_no = new String[78];
		session.setAttribute("select_no", select_no);

		//一問目のデータをDAOでDBから取得して、Questionsインスタンスのフィールドに格納
		//MEMO QuestionsインスタンスがDBテーブルの1レコードに対応する
		Questions questions  = new Questions(category_id_int,nextLevel_int) ;
		QuestionsDAO dao = new QuestionsDAO();
		Questions dao_questions = dao.findByQuestions(questions);

		//questions.jspで表示する選択肢をListにして取得
		ChoicesListDAO c = new ChoicesListDAO();
		List<Choices>choicesList = c.findByChoicesList(dao_questions);

		//必要な値をリクエストスコープにセットしてquestions.jspに遷移
		request.setAttribute("questions", dao_questions);
		request.setAttribute("choicesList", choicesList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/questions.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

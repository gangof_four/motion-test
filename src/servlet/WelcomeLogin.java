package servlet; //asdf

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AccountDAO;
import model.Account;
import model.Login;
import model.LoginLogic;

/**
 * Servlet implementation class WelcomeLogin
 */
@WebServlet("/WelcomeLogin")
public class WelcomeLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WelcomeLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//初期画面（ログイン画面）へ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//ログイン画面で入力したパラメーターを受け取る
		String user_id = request.getParameter("login_id");
		String pass = request.getParameter("pass");

		//Loginインスタンスを生成
		Login user = new Login(user_id, pass);
		//ログイン結果判定(executeメソッドでuser_idとpassが一致したアカウントがDB上に存在しているか判定している
		LoginLogic loginLogic = new LoginLogic();
		boolean isLogin = loginLogic.execute(user);

		//ログイン成功ならセッションインスタンスloginUserを生成してダッシュボード（ユーザー画面）へ遷移
		//ログイン失敗ならリクエストインスタンスnoUserを生成して再度ログイン画面へ
		if (isLogin) {
			AccountDAO tmp = new AccountDAO();
			Account loginUser = tmp.findByLogin(user);
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", loginUser);
		}
		else {
			request.setAttribute("noUser", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/loginUser.jsp");
		dispatcher.forward(request, response);
	}
}

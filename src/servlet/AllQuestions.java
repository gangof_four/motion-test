package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.QuestionsListDAO;
import model.Questions;

/**
 * Servlet implementation class AllQuestions
 */
@WebServlet("/AllQuestions")
public class AllQuestions extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllQuestions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//response.getWriter().append("Served at: ").append(request.getContextPath());

		//一覧画面から問題画面に戻った時に問題番号が変わらないようにリクエストスコープでlevelを渡していく
		//HACK 毎回リクエストスコープで渡していく以外にもっとシンプルな方法があるかもしれない
		String nowLevel = request.getParameter("nowLevel");
		int nowLevel_int = Integer.parseInt(nowLevel);

		HttpSession session = request.getSession();
		int category_id = (int) session.getAttribute("category_id");

		//問題の一覧を表示するために全レコードをQuestionsインスタンス化してListに格納
		Questions questions = new Questions(category_id,nowLevel_int);
		QuestionsListDAO q_tmp = new QuestionsListDAO();
		List<Questions>questionsList = q_tmp.findByQuestionsList(questions);

		//レビューチェックした場合に✔を配列に格納
		//MEMO 問題番号levelとインデックス番号level_index--が対応
		/*String[] review = (String[]) session.getAttribute("review");
		int level_index = nowLevel_int;
		if(request.getParameter("review_check") != null) {
			review[level_index] = "✔";
		}else {
			review[level_index] = null;
		}*/


		request.setAttribute("questionsList", questionsList);
		request.setAttribute("nowLevel", nowLevel);

		//all_questions.jspに遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("/all_questions.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

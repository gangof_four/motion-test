package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CorrectAnswerNumberUpdate;
import dao.QuestionsListDAO;
import dao.TestResultsDAO;
import model.Account;
import model.Questions;
import model.TestResults;

/**
 * Servlet implementation class FinishQuestions
 */
@WebServlet("/FinishQuestions")
public class FinishQuestions extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FinishQuestions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String nowLevel = request.getParameter("nowLevel");

		request.setAttribute("nowLevel", nowLevel);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/finish_con.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);


		HttpSession session = request.getSession();
		String[] select_no = (String[]) session.getAttribute("select_no");
		int category_id = (int) session.getAttribute("category_id");
		Account loginUser = (Account) session.getAttribute("loginUser");



		Questions questions = new Questions(category_id);
		QuestionsListDAO tmp = new QuestionsListDAO();
		List<Questions> questionsList = tmp.findByQuestionsList(questions);

		int correct_answer_number = 0;
		int i = 1;
		List<Integer> mistake_level = new ArrayList<>();

		//正解数(correct_answer_number)が増えていく
		//不正解だった場合、その問題番号をmistake_levelに格納
		for(Questions q : questionsList) {
			String select = select_no[i];
			String answer = q.getCorrect_answer_id();
			if(answer.equals(select)) {
				correct_answer_number++;
			}else {
				mistake_level.add(q.getLevel());
			}
			i++;
		}

		//正解数をTestResultsで更新（update)
		TestResults tr = new TestResults(loginUser.getId(),category_id);
		CorrectAnswerNumberUpdate answer_number_update = new CorrectAnswerNumberUpdate();
		answer_number_update.updateCorrectAnswerNumber(tr, correct_answer_number);
		TestResultsDAO trdao = new TestResultsDAO();
		TestResults test_results = trdao.findByTestResults(tr);

		//正解数を問題数で割って、正解率を取得
		int correct_answer_rate =  100*test_results.getCorrect_answer_number() / test_results.getQuestions_number();


		request.setAttribute("correct_answer_rate", correct_answer_rate);
		request.setAttribute("mistake_level", mistake_level);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/finish_questions.jsp");
		dispatcher.forward(request, response);


	}

}

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Questions;

public class QuestionsListDAO {
	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public List<Questions> findByQuestionsList(Questions questions) {
		List<Questions> questionsList = new ArrayList<>();
		Questions dao_questions = null;
		int id;
		int category_id;
		int level;
		String contents;
		String annotation;
		String commentary;
		String correct_answer_id;
		String modified_on;
		String created_on;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);


			//指定した章の問題を全て取得
			String sql = "SELECT * FROM questions WHERE category_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, questions.getCategory_id());

			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				id = rs.getInt("id");
				category_id = rs.getInt("category_id");
				level = rs.getInt("level");
				contents = rs.getString("contents");
				annotation = rs.getString("annotation");
				commentary = rs.getString("commentary");
				correct_answer_id = rs.getString("correct_answer_id");
				modified_on = rs.getString("modified_on");
				created_on = rs.getString("created_on");


				dao_questions = new Questions(id,category_id,level,contents,annotation,commentary,
						correct_answer_id,modified_on,created_on);
				questionsList.add(dao_questions);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return questionsList;
	}
}

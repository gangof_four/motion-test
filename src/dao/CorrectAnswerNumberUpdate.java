package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.TestResults;

public class CorrectAnswerNumberUpdate {
	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public void updateCorrectAnswerNumber(TestResults test_results, int correct_answer_number) {

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);


			String sql = "UPDATE test_results SET correct_answer_number = ? WHERE user_id = ? AND category_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, correct_answer_number);
			pStmt.setInt(2,  test_results.getUser_id());
			pStmt.setInt(3, test_results.getCategory_id());

			pStmt.executeUpdate();

			//conn.commit();


		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

	}
}

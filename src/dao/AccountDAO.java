package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Account;
import model.Login;

public class AccountDAO {


	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public Account findByLogin(Login login) {
		Account account = null;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);

			String sql = "SELECT id,login_id, password FROM users WHERE login_id = ? AND password = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login.getUserId());
			pStmt.setString(2,  login.getPass());

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				//MEMO 取得カラムは必要最低限にしている（必要に応じてSELECT文と合わせて足す）
					int id = rs.getInt("id");
					String login_id = rs.getString("login_id");
					String pass = rs.getString("password");

				account = new Account(id,login_id,pass);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return account;
	}
}

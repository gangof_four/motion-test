package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.TestResults;

public class TestResultsDAO {
	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public TestResults findByTestResults(TestResults test_results) {
		TestResults tr = null;
		int user_id = 0;
		int category_id = 0;
		int questions_number = 0;
		int correct_answer_number = 0;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);


			String sql = "SELECT user_id,category_id,questions_number,correct_answer_number FROM test_results WHERE user_id = ? AND category_id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, test_results.getUser_id());
			pStmt.setInt(2, test_results.getCategory_id());

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				//MEMO 取得カラムは必要最低限にしている（必要に応じてSELECT文と合わせて足す）
				user_id = rs.getInt("user_id");
				category_id = rs.getInt("category_id");
				questions_number = rs.getInt("questions_number");
				correct_answer_number = rs.getInt("correct_answer_number");

				tr = new TestResults(user_id,category_id,questions_number,correct_answer_number);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return tr;
	}
}

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Account;
import model.Login;

public class SqlConnectDAO {
	private final String JDBC_URL = "jdbc:mysql://127.0.0.1/db_sample?characterEncoding=UTF-8&serverTimezone=JST";
	//private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "root";
	private final String DB_PASS = "";

	PreparedStatement pStmt = null;

	public Account findByLogin(Login login) {
		Account account = null;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);

			//todo : select文を正しく変える
			String sql = "SELECT name, id FROM m_student WHERE name = ? AND id = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login.getUserId());
			pStmt.setString(2,  login.getPass());

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
					//String userId = rs.getString("login_id");
					//String pass = rs.getString("password");

				String name = rs.getString("name");
				String pass = rs.getString("id");
				//String maker = rs.getString("maker");

				//out.println("<p>name:" + name + "</p>");
				//out.println("<p>maker:" + maker + "</p>");

				account = new Account(name,pass);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}
				if(pStmt != null) {
					pStmt.close();
				}
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return account;
	}
}

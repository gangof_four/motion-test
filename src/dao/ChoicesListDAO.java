package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Choices;
import model.Questions;

public class ChoicesListDAO {
	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public List<Choices> findByChoicesList(Questions questions) {
		List<Choices> choicesList = new ArrayList<>();
		Choices dao_choices = null;
		String contents = null;
		String no = null;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);

			String sql = "SELECT id,question_id,no, contents FROM choices WHERE question_id = ?";
			pStmt = conn.prepareStatement(sql);
			//QuestionsServletから受け取ったquestions.id
			pStmt.setInt(1, questions.getId());

			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				//MEMO 取得カラムは必要最低限にしている（必要に応じてSELECT文と合わせて足す）
				int id = rs.getInt("id");
				int question_id = rs.getInt("question_id");
				no = rs.getString("no");
				contents = rs.getString("contents");

				//TODO ChoicesインスタンスをArrayListに格納（追加）
				dao_choices = new Choices(id,question_id,no, contents);
				choicesList.add(dao_choices);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return choicesList;
	}
}

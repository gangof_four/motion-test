package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Questions;

public class QuestionsDAO {

	private final String JDBC_URL = "jdbc:mysql://192.168.69.161/webexam?characterEncoding=UTF-8&serverTimezone=JST";
	private final String DB_USER = "testuser";
	private final String DB_PASS = "1234";

	PreparedStatement pStmt = null;

	public Questions findByQuestions(Questions questions) {
		Questions dao_questions = null;
		int id = 0;
		int category_id = 0;
		String contents = null;
		int level = 0;

		try{

			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS);


			String sql = "SELECT id,category_id,contents,level FROM questions WHERE category_id = ? AND level = ?";
			pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, questions.getCategory_id());
			pStmt.setInt(2, questions.getLevel());

			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				//MEMO 取得カラムは必要最低限にしている（必要に応じてSELECT文と合わせて足す）
				id = rs.getInt("id");
				category_id = rs.getInt("category_id");
				contents = rs.getString("contents");
				level = rs.getInt("level");

				dao_questions = new Questions(id,category_id,contents,level);

			}

		}catch(SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {

				if(pStmt != null) {
					pStmt.close();
				}

			}catch(SQLException e) {
				e.printStackTrace();
			}
		}

		return dao_questions;
	}

}

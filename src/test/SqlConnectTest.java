package test;

import dao.SqlConnectDAO;
import model.Account;
import model.Login;

public class SqlConnectTest {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		testFindByLogin1();
		testFindByLogin2();
	}
	public static void testFindByLogin1() {
		//todo : ユーザーIDとpass 後で書く
		Login login = new Login("Shimazaki Minako", "1");
		SqlConnectDAO dao = new SqlConnectDAO();
		Account result = dao.findByLogin(login);

		if(result != null &&
				result.getUserId().contentEquals("Shimazaki Minako") &&
				result.getPass().contentEquals("1")){
			System.out.println("testFindByLogin1:成功しました");
		}else {
			System.out.println("testFindByLogin1:失敗しました");
		}
	}

	public static void testFindByLogin2() {
		Login login = new Login("Shimazaki Minako", "12345");
		SqlConnectDAO dao = new SqlConnectDAO();
		Account result = dao.findByLogin(login);
		if(result == null) {
			System.out.println("testFindByLogin2:成功しました");
		}else {
			System.out.println("testFindByLogin2:失敗しました");
		}
	}
}

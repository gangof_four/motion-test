package model;

public class Account {
	private int id;
	private String userId;
	private String lastName;
	private String lastNameKana;
	private String firstName;
	private String firstNameKana;
	private String loginId;
	private String pass;
	private String modifiedOn;
	private String createdOn;

	//MEMO 必要に応じて引数の異なるコンストラクタを用意している
	public Account(String loginId, String pass) {
		this.loginId = loginId;
		this.pass = pass;
	}

	public Account(int id,String loginId, String pass) {
		this.id = id;
		this.loginId = loginId;
		this.pass = pass;
	}

	public Account(String userId, String lastName, String lastNameKana, String firstName, String firstNameKana,
			String loginId, String pass, String modifiedOn, String createdOn) {
		this.userId = userId;
		this.lastName = lastName;
		this.lastNameKana = lastNameKana;
		this.firstName = firstName;
		this.firstNameKana = firstNameKana;
		this.loginId = loginId;
		this.pass = pass;
		this.modifiedOn = modifiedOn;
		this.createdOn = createdOn;
	}

	public String getUserId() {
		return userId;
	}

	public String getLastName() {
		return lastName;
	}

	public String getLastNameKana() {
		return lastNameKana;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getFirstNameKana() {
		return firstNameKana;
	}

	public String getLoginId() {
		return loginId;
	}

	public String getPass() {
		return pass;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public int getId() {
		return id;
	}


}

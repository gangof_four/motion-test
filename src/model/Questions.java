package model;

public class Questions {
	private int id;
	private int category_id;
	private int level;
	private String contents;
	private String annotation;
	private String commentary;
	private String correct_answer_id;
	private String modified_on;
	private String created_on;

	//MEMO 必要に応じて引数の異なるコンストラクタを用意している
	public Questions(int id,int category_id,int level,String contents,String annotation,String commentary,
						String correct_answer_id,String modified_on,String created_on) {
		this.id = id;
		this.category_id = category_id;
		this.level = level;
		this.contents = contents;
		this.annotation = annotation;
		this.commentary = commentary;
		this.correct_answer_id = correct_answer_id;
		this.modified_on = modified_on;
		this.created_on = created_on;

	}

	public Questions(int id,int category_id,String contents,int level) {
		this.id = id;
		this.category_id = category_id;
		this.contents = contents;
		this.level = level;
	}

	public Questions(int category_id,int level) {
		this.category_id = category_id;
		this.level = level;
	}

	public Questions(int category_id) {
		this.category_id = category_id;
	}

	public int getId() {
		return id;
	}

	public int getLevel() {
		return level;
	}

	public int getCategory_id() {
		return category_id;
	}

	public String getContents() {
		return contents;
	}

	public String getAnnotation() {
		return annotation;
	}

	public String getCommentary() {
		return commentary;
	}

	public String getCorrect_answer_id() {
		return correct_answer_id;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}

}

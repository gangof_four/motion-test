package model;

public class Choices {
	private int id;
	private int question_id;
	private String no;
	private String contents;
	private String modified_on;
	private String created_on;

	//MEMO 必要に応じて引数の異なるコンストラクタを用意している
	public Choices(int id,int question_id, String no, String contents) {
		this.id = id;
		this.question_id = question_id;
		this.no = no;
		this.contents = contents;
	}

	public int getId() {
		return id;
	}

	public int getQuestion_id() {
		return question_id;
	}

	public String getContents() {
		return contents;
	}

	public String getNo() {
		return no;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}

}

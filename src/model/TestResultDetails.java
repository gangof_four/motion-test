package model;

public class TestResultDetails {
	private String id;
	private String test_result_id;
	private String no;
	private int question_id;
	private String answer_choice_no;
	private String correct_answer_id;
	private String contents;
	private String annotation;
	private String commentary;
	private String choice_contents_1;
	private String choice_contents_2;
	private String choice_contents_3;
	private String choice_contents_4;
	private String choice_contents_5;
	private String choice_contents_6;
	private String choice_contents_7;
	private String choice_contents_8;
	private String start_time;
	private String ending_time;
	private String modified_on;
	private String created_on;

	//MEMO 必要に応じて引数の異なるコンストラクタを用意している
	public TestResultDetails(int question_id,String answer_choice_no, String correct_answer_id){
		this.question_id = question_id;
		this.answer_choice_no = answer_choice_no;
		this.correct_answer_id = correct_answer_id;
	}


	public String getId() {
		return id;
	}

	public String getTest_result_id() {
		return test_result_id;
	}

	public String getNo() {
		return no;
	}

	public int getQuestion_id() {
		return question_id;
	}

	public String getAnswer_choice_no() {
		return answer_choice_no;
	}

	public String getCorrect_answer_id() {
		return correct_answer_id;
	}

	public String getContents() {
		return contents;
	}

	public String getAnnotation() {
		return annotation;
	}

	public String getCommentary() {
		return commentary;
	}

	public String getChoice_contents_1() {
		return choice_contents_1;
	}

	public String getChoice_contents_2() {
		return choice_contents_2;
	}

	public String getChoice_contents_3() {
		return choice_contents_3;
	}

	public String getChoice_contents_4() {
		return choice_contents_4;
	}

	public String getChoice_contents_5() {
		return choice_contents_5;
	}

	public String getChoice_contents_6() {
		return choice_contents_6;
	}

	public String getChoice_contents_7() {
		return choice_contents_7;
	}

	public String getChoice_contents_8() {
		return choice_contents_8;
	}

	public String getStart_time() {
		return start_time;
	}

	public String getEnding_time() {
		return ending_time;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}

}

package model;

public class TestTypes {
	private int id;
	private String name;
	private String detail;
	private int question_number;
	private int test_time;
	private String modified_on;
	private String created_on;

	//TODO コンストラクタ作成


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}

	public int getQuestion_number() {
		return question_number;
	}

	public int getTest_time() {
		return test_time;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}



}

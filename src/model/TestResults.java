package model;

public class TestResults {
	private int id;
	private int user_id;
	private int test_type_id;
	private int category_tied;
	private int category_id;
	private int questions_number;
	private int correct_answer_number;
	private String start_time;
	private String ending_time;
	private String modified_on;
	private String created_on;

	//TODO コンストラクタ作成
	public TestResults(int user_id,int category_id,int questions_number,int correct_answer_number){
		this.user_id = user_id;
		this.category_id = category_id;
		this.questions_number = questions_number;
		this.correct_answer_number = correct_answer_number;
	}

	public TestResults(int user_id,int category_id) {
		this.user_id = user_id;
		this.category_id = category_id;
	}


	public int getId() {
		return id;
	}

	public int getUser_id() {
		return user_id;
	}

	public int getTest_type_id() {
		return test_type_id;
	}

	public int getCategory_tied() {
		return category_tied;
	}

	public int getCategory_id() {
		return category_id;
	}

	public int getQuestions_number() {
		return questions_number;
	}

	public int getCorrect_answer_number() {
		return correct_answer_number;
	}

	public String getStart_time() {
		return start_time;
	}

	public String getEnding_time() {
		return ending_time;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}
}

package model;

public class Categories {
	private int id;
	private String name;
	private String detail;
	private String contents;
	private String modified_on;
	private String created_on;

	//TODO　コンストラクタ作成


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}

	public String getContents() {
		return contents;
	}

	public String getModified_on() {
		return modified_on;
	}

	public String getCreated_on() {
		return created_on;
	}

}

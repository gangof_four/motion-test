<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.List,model.Questions,dao.QuestionsListDAO" %>
    <% String nowLevel = (String) request.getAttribute("nowLevel");
    		List<Questions> questionsList = (List<Questions>) request.getAttribute("questionsList");
    		String[] review = (String[]) session.getAttribute("review");
    		String[] select_no = (String[]) session.getAttribute("select_no");
    		String finish_message = (String) request.getAttribute("finish_message");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>問題一覧</title>
</head>
<body>

<% if(finish_message != null){
	out.print(finish_message);
}
%>
<br>
<%
for(Questions q : questionsList){
	int thisLevel = q.getLevel();
	%>
	<form action="/Web_exam/QuestionsServlet" method="post">
	<input type="hidden" name="thisLevel"  value=<%= thisLevel %>>
	<INPUT type="submit" value=<%= thisLevel %>>
	</form>
	<%
	String contents = q.getContents();
	out.print(contents.substring(0,20) + "...");

	out.print(review[thisLevel]);
	//未回答問題が分かるようにチェックを表示
	if(select_no[thisLevel] ==null){
		out.print("✔");
	}else{
		out.print(select_no[thisLevel]);
	}


	%>
	<br>
	<%
}
%>


<br>

<%-- TODO  問題画面（questions.jsp)に遷移する必要がある --%>
<form action="/Web_exam/QuestionsServlet" method="post">
<input type="hidden"  name="nowLevel" value=<%= nowLevel %>>
<INPUT type="submit"  value="戻る">
</form>
<form action="/Web_exam/FinishQuestions" method="get">
<input type="hidden"  name="nowLevel" value=<%= nowLevel %>>
<INPUT type="submit"  value="終了">
</form>

</body>
</html>
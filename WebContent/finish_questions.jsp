<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ page import="java.util.List,model.Questions" %>
    <% int correct_answer_rate = (int) request.getAttribute("correct_answer_rate");
    	List<Integer> mistake_level = (List<Integer>) request.getAttribute("mistake_level");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>解答終了</title>
</head>
<body>

<p>お疲れさまでした</p><br>
<p>正解率:
<%
out.print(correct_answer_rate);
out.print("%");
%>
</p><br>

<%
out.println("間違えた問題番号");
for(Integer m : mistake_level){
	out.print(m);
	out.print(",");
}
%>

<form action="/Web_exam/ReturnDashboard" method="get">
<INPUT type="submit"  value="ダッシュボードに戻る">
</form>
</body>
</html>
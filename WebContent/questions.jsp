<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.Date,java.util.List,model.Questions,model.Choices" %>
    <%
    Date start = (Date) session.getAttribute("start");
    String[] review = (String[]) session.getAttribute("review");
    Questions questions = (Questions) request.getAttribute("questions");
    List<Choices> choicesList = (List<Choices>) request.getAttribute("choicesList");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>試験</title>
</head>
<body>
<p>${loginUser.loginId}さんがログイン中<p/><br>
<p>
残り時間(分）：
<%-- 放置してるとなぜか増えて150越えする！要チェック！ --%>
<%-- LocalTime nowtime = LocalTime.now(); --%>
<% Date now = new Date();
long start_min = ((start.getTime() % (1000 * 60 * 60)) / 1000 / 60) % 60;
long now_min = ((now.getTime() % (1000 * 60 * 60)) / 1000 / 60) % 60;
%>
<%--  150 - (nowtime.getMinute() - start.getMinute())--%>
<%= 150 - (now_min - start_min) %>
</p><br>

<p>
問<%= questions.getLevel() %>
</p><br>


<%-- 問題一覧へ遷移 --%>
<form action="/Web_exam/AllQuestions" method="get">
<!--  <p>レビュー<input type="checkbox" name="review_check" ></p> -->
<input type="hidden"  name="nowLevel" value=<%= questions.getLevel() %>>
<INPUT type="submit"  value="一覧へ">
</form>

<form action="/Web_exam/QuestionsServlet"  method="get">

<%-- 問題を表示 --%>
<p><%= questions.getContents().replace("\r\n","<br>") %></p> <br>


<%
for(Choices c : choicesList){
	%>
	<input type="checkbox" name="choice_no" value=<%= c.getNo() %>>
	<%
	out.print(c.getNo());
	out.print(". ");
	out.print(c.getContents().replace("\r\n","<br>"));
	%>
	<br>
	<%
}
%>


<%-- TODO 不安な問題チェック --%>
<p>レビュー<input type="checkbox" name="review_check" ></p>

<input type="hidden"  name="nextLevel" value=<%= questions.getLevel() %>>
<INPUT type="submit"  value="次へ">
</form>

</body>
</html>
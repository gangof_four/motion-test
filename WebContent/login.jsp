<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.Login" %>
<%
//ログインに失敗した場合のみ取得するインスタンス
Login noUser = (Login) request.getAttribute("noUser");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
</head>
<body>
<p><%
if(noUser != null){
	out.print("ログインに失敗しました");
}
%></p>
<p class="p1">ログインIDとパスワードを入力してください。</p>
<%-- フォームはページに入力したデータをサーバサイドプログラムに送信する
action属性：送信先を指定する　method属性：リクエストメソッドを指定する --%>
<form action="/Web_exam/WelcomeLogin" method="post">
    <table>
    <tr>
        <td>ログインID</td><td>：<input type="text" name="login_id"></td>
    </tr>
    <tr>
        <td>パスワード</td><td>：<input type="text" name="pass"></td>
    </tr>
    </table>
    <p><input type="submit" value="チェック実行" style="WIDTH: 200px; HEIGHT: 20px"></p>
</form>

</body>
</html>
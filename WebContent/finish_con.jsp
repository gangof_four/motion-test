<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <% String nowLevel = (String) request.getAttribute("nowLevel"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>終了確認</title>
</head>
<body>
<p>本当に終了してもよろしいですか？</p><br>

<form action="/Web_exam/FinishQuestions" method="post">
<INPUT type="submit" value="はい">
</form>

<form action="/Web_exam/AllQuestions" method="get">
<input type="hidden"  name="nowLevel" value=<%= nowLevel %>>
<INPUT type="submit" value="いいえ、問題に戻ります">
</form>

</body>
</html>
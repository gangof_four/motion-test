<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.Date, java.text.DateFormat" %>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="loginUser.css">
<meta charset="UTF-8">
<title>ダッシュボード</title>
</head>
<body>
<p><%= new java.util.Date()%></p>
<br>
<p>${loginUser.loginId}さんがログイン中<p/>
<h1>模擬試験を解く</h1><br>

<% for(int i = 1; i < 12; i++){
	out.print("試験" + (i)); %>
	<%-- TODO category_idとして指定する値を模試用の値に変更（i+9から？） --%>
	<form action="/Web_exam/FirstQuestions" method="get">
	<input type="hidden" name="category_id" value="<%= i %>">
	<input type="hidden"  name="nextLevel"  value=1>
	<INPUT style="margin:20px; " type="submit"  value= "開始" >
	</form>
	<% out.print("前回の実施日は○日前です。");%>
	<br>
<% } %>

<!--  現在、表示はされるがカスタマイズ問題の機能は実装していない -->
<h1>カスタマイズ問題を解く</h1>
<p>出題数</p>
<select class =”size”>
<option value=”5”>5</option>
<option value=”10”>10</option>
<option value=”15”>15</option>
<option value=”20”>20</option>
<option value=”25”>25</option>
<option value=”30”>30</option>
<option value=”40”>40</option>
<option value=”50”>50</option>
<option value=”60”>60</option>
<option value=”70”>70</option>
</select>
<br>

<p>出題範囲</p>
<select class =”category”>
<option value=”全ての範囲”>全ての範囲</option>
<option value=”Javaの基本”>Javaの基本</option>
<option value=”データ型の操作”>データ型の操作</option>
<option value=”演算子と判定構造の使用”>演算子と判定構造の使用</option>
<option value=”配列の作成と使用”>配列の作成と使用</option>
<option value=”ループ構造の使用”>ループ構造の使用</option>
<option value=”メソッドとカプセル化の操作”>メソッドとカプセル化の操作</option>
<option value=”継承の操作”>継承の操作</option>
<option value=”例外の処理”>例外の処理</option>
<option value=”JavaAPIの主要なクラスの操作”>JavaAPIの主要なクラスの操作</option>
</select>
<br>

<p>問題範囲</p>
<select class =”question”>
<option value=”全ての範囲”>全ての範囲</option>
<option value=”不正解問から”>不正解問から</option>
</select>
<br>
<INPUT type="submit" name="botan1" value="開始">
<br>
<br>
<form action="/Web_exam/Logout" method="get">
<INPUT type="submit" name="botan1" value="ログアウト">
</form>

</body>
</html>